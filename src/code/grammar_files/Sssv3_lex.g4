grammar Sssv3_lex;
import  Sssv3_frags;

Meta_data  
    : METADATA
    ;

Pg_xform
    : PGXFORM
    ;

// loc identifies which element(s) are selecte
Loc
  : LOC
  ;

// action to be performed on the selected web page item
Action
   : ADD 
   | DEL
   | REPL
   | MOD
   | PRCS
   | ADD_TT   // add-tt (for tooltip)
   | ADD_YT   // add-yt (for youtube)
   | ADD_IMG  // add-img (for img)
   | ADD_TXT  // add-txt (for text)
   | ADD_LINK // add-link (for hyperlink)
   | ADD_CSS  // add-css (for a full css file)
   | ADD_STYLE // add-style (for style for a earmarked element)
   | ADD_HTML  // add-html
   | ADD_SCRIPT // add-script
   | REPL_LINK  // repl-link
   | REPL_TXT   // repl-txt
   | REPL_IMG   // repl-img
   | MOD_ELM   // modify html tag related properties
   | REASMB  // Reassembly of a page
   ;

Cond
    : COND
    ;

Sss_crite
    : SSSCRITE
    ;

// attribute value pair used in all the segments
Attr
// SSS attributes, typically used in seg1
// MD_Attr
    :    CURRATOR_NAME // Name of the sss creator
    |    CURRATOR_ID   // ID of the sss creator
    |    SSS_NAME     // Name of the SSS file
    |    SSS_ID       // ID of the SSS file
    |    SSS_DESC     // description of the sss file
    |    RENURL
// Loc_attr
    |    TAG
    |    NODE
    |    CSTAG
// Actn_attr value pair used in actions   
    |   SRC_LOC  // source location    
    |   NEW_TEXT
    |   ADD_LOC  
    |   FROM_TYPE 
    |   TO_TYPE
    |   TEXT
    |   LANG       // typically used for translations
    |   NUM_FM
    |   CRNCY_FM   // currency format
    |   TIME_FM    // format for displaying time
    |   UNIT_FM    // format for display of units of measure
    |   COLOR
    |   CONV_RATE  // forex conversion rate  
    |   CSTM_DATA         // to be used by custom event-handlers
    |   CSTM_SRC          // to be used as event handlers for custom tags
    |   CSTM_HNDLR_NAME // the script text that we need to use in dynamic binding of custom event handlers
    // attributes for MODIFY function
    |   ADD_CLASS       // adds another class element to earmarked HTML tagged element 
    |   MOD_CLASS       // replaces the existing tag class with specified class 
    |   MOD_ID          // replaces the existing ID with new ID for earmarked HTML element 
// Cond_attr attribute value pair used in actions     
    |   TOD         // Time of Day
    |   BROWSER		// browser information triggered selection
// Selctn_attr attribute value pair used in seg3 selection criteria
    |   USR_NAME        // Name of the user to which the sss should be shown 
    |   USR_ID          // Id of the user to which the sss should be shown
    |   CMNTY_NAME      // Community name to which the sss should be shown
    |   CMNTY_ID        // Community ID to which the sss should be shown
    ;

Value
   : STRING
   | NUMBER
   | 'True'
   | 'False'
   | 'Null'
   ;

STRING
   : '"' (ESC | ~ ["\\])* '"'           
   ;

NUMBER
   : '-'? INT '.' [0-9] + EXP? | '-'? INT EXP | '-'? INT
   ;

WS
    : [ \t\n\r] + -> skip
    ;

LINE_COMMENT
    : '//' ~[\r\n]* -> skip
    ;

