import json
import requests
import conf
import pprint
import sys

validation_data = json.load(open("../json/sss_validation.json"))

# This class is used to create a sss "abstract data model"
class SssModel():
	sssCount = 0
	def __init__(self):
		SssModel.sssCount += 1

	def setSss(self):
		self.sss = {}
	
	def getSssCount(self):
		print("Total SSS  count %d" % SssModel.sssCount)
	
	def setSeg1(self, seg1=None):
                try:
                        if seg1:
                                self.sss[seg1] = {}
                except:
                        print("Error: seg1 parameter is not passed")
                else:
                        print("Created seg1...!")
	
	def setSeg2(self, seg2=None):
                try:
                        if seg2:
                                self.sss[seg2] = []
                except:
                        print("Error: seg2 parameter is not passed")
                else:
                        print("Created seg2...!")	
	
	def setSeg3(self, seg3=None):
                try:
                        if seg3:
                                self.sss[seg3] = {}

                except:
                        print("Error: seg3 parameter is not passed")
                else:
                        print("Created seg3...!")
	
	def setSeg1Attr(self, seg1=None, attr=None, value=None):
                try:
                        if seg1 and attr and value:
                                self.sss[seg1][attr] = value
                except:
                        print("Error: seg1 attr, value paramaters are not passed")
                else:
                        print("Populated seg1 with attr, value pairs...!")
	
	def setSeg3Attr(self, seg3=None, attr=None, value=None):
                try:
                        if seg3 and attr and value:
                                self.sss[seg3][attr] = value

                except:
                        print("Error: seg3 attr, value paramaters are not passed")
                else:
                        print("Populated seg3 with attr, value pairs...!")
	
	def setSeg2Stmnts(self, loc=None, acpair=None):
                try:
                        self.seg2_stmnt = {}
                        if loc and acpair:
                                self.seg2_stmnt[loc] = {}
                                self.seg2_stmnt[acpair] = []

                except:
                        print("Error in passing the seg2 statement paramaters(loc or acpair)")
                else:
                        print("Created seg2_stmnt and populated loc and acpair stmnts...!") 
	
	def setLocAttr(self, loc=None, attr=None, value=None):
                try:
                        if loc and attr and value:
                                self.seg2_stmnt[loc][attr] = value

                except:
                        print("Error in passing location, attr, value parameters")
                else:
                        print("Populated loc with attr, value pairs...!")
	
	def setAcStmnt(self, action=None, cond=None):
                try:
                        self.ac_stmnt =	{}
                        if action and cond:
                                self.ac_stmnt[action] = {}
                                self.ac_stmnt[cond] = {}
                        elif action:
                                self.ac_stmnt[action]	={}

                except:
                        print("Error in passing the action,condition statement paramaters")

                else:
                        print("Created ac_stmnt dict and populated stmnts(action and cond)...!")
	
	def setActionAttr(self, action=None, attr=None, value=None):
                try:
                        if action and attr and value:
                                self.ac_stmnt[action][attr] = value

                except:
                        print("Error in passing action attr,value paramaters")
                else:
                        print("Populated action with attr, value pairs...!")
	
	def setCondAttr(self, cond=None, attr=None, value=None):
                try:
                        if cond and attr and value:
                                self.ac_stmnt[cond][attr] = value

                except:
                        print("Error in passing condition attr,value paramaters")
                else:
                        print("Populated cond with attr, value pairs...!")
	
	def setActnCond(self):
                self.seg2_stmnt["acpair"].append(self.ac_stmnt)
			
	def setFinalSeg2(self, seg2=None):
		self.sss[seg2].append(self.seg2_stmnt)

	def getSss(self):
   		return self.sss

# This class is used to validate sss abstract data model
class validateSss():
        def __init__(self, dict):
                self.sss_model = dict
                """
                Calls methods to validate sss segements `seg1`(metadata segment),
                `seg2`(pgxform), `seg3`(ssscrite)
                """
                self.seg1Validation(self.sss_model)
                self.seg2Validation(self.sss_model)
                self.seg3Validation(self.sss_model)

        def seg1Validation(self, model):
                """
                Checks if all the required attributes are present in `metadata`
                segment by taking the `sss_validation.json` as reference for 
                validation.
                """
                try:
                        seg1_req_attrs = set(validation_data["metadata"]["required"]).issubset(model["metadata"])
                except NameError:
                        sys.exit("seg1 Error: validation_data identifier not found")
                except KeyError:
                        sys.exit("seg1 Error: Key not found in validation_data object")
                except RuntimeError:
                        sys.exit("Error in seg1validation")
                else:
                        if(seg1_req_attrs == False):
                                sys.exit("Error in seg1 validation: Required attribute doesn't exist in metadata segment")

                        elif(seg1_req_attrs == True):
                                """
                                Checks if only possible attributes exists in `metadata` segment by 
                                taking the `sss_validation.json` as reference for validation.
                                """
                                for key in model["metadata"]:
                                        if(key not in validation_data["metadata"]["all"]):
                                                sys.exit("Error in seg1 validation: Unknown attribute"+" "+key+" "+"in metadata segment")
                return print("seg1(Metadata) validated successfully...!")


        def seg2Validation(self, model):
                """
                Checks the possible `loc` attributes 
                """
                try:
                        for stmnt_index in range(len(model["pgxform"])):
                                for key in model["pgxform"][stmnt_index]['loc']:
                                        if(key not in validation_data["pgxform"]["loc-attr"]):
                                                sys.exit("Error: Unknown attribute"+" "+key+" "+"in `loc`")
                                """
                                Validates `action` and `condition` attributes
                                """
                                for acstmnt_index in range(len(model["pgxform"][stmnt_index]["acpair"])):
                                        for key in model["pgxform"][stmnt_index]["acpair"][acstmnt_index]:
                                                if(key == "cond"):
                                                        for condkey in model["pgxform"][stmnt_index]["acpair"][acstmnt_index][key]:
                                                                if(condkey not in validation_data["pgxform"]["cond-attr"]):
                                                                        sys.exit("Error: Unknown attribute"+" "+condkey+" "+"in"+" "+key)
                                                if(key == "addtxt"):
                                                        for actnkey in model["pgxform"][stmnt_index]["acpair"][acstmnt_index][key]:
                                                                if(actnkey not in validation_data["pgxform"][key+"-attr"]):
                                                                        sys.exit("Error: Unknown attribute"+" "+actnkey+" "+"in"+" "+key)
                
                except NameError:
                        sys.exit("seg2 Error: validation_data identifier not found")
                except KeyError:
                        sys.exit("seg2 Error: Key not found in validation_data object")
                except RuntimeError:
                        sys.exit("Error in seg2validation")
                return print("seg2(pgxform) validated successfully...!")


        def seg3Validation(self, model):
                """
                Checks the possible attributes in `ssscrite` segment by 
                taking the `sss_validation.json` as reference for validation.
                """
                try:
                        for key in model["ssscrite"]:
                                if(key not in validation_data["ssscrite"]["all"]):
                                        sys.exit("Error: Unknown attribute"+" "+key+" "+"in ssscrite segment")
                except NameError:
                        sys.exit("seg3 Error: validation_data identifier not found")
                except KeyError:
                        sys.exit("seg3 Error: Key not found in validation_data object")
                except RuntimeError:
                        sys.exit("Error in seg3validation")
                return print("seg3(ssscrite) validated successfully...!")

# This class is used to persist the sss abstract data model in database
class saveSss:
    def __init__(self, sss):
        self.sss_dict = sss
        self.sendJson(self.sss_dict)
        
    def sendJson(self, dict):
      try:
        """
        Populates the sss in database 
      	"""
        body = dict
        endpoint = 'http://'+conf.DATABASE_HOSTNAME[0]+':'+conf.DATABASE_PORT[0]+'/api/update/sss/'
        headers = {'Content-Type': 'application/json'}
        db_response = requests.post(endpoint, data=json.dumps(body), headers=headers, timeout=100)
        db_response.raise_for_status()
      except requests.exceptions.HTTPError as errh:
        print ("Http Error:",errh)
      except requests.exceptions.ConnectionError as errc:
        print ("Error Connecting:",errc)
      except requests.exceptions.Timeout as errt:
        print ("Timeout Error:",errt)
      except requests.exceptions.RequestException as err:
        print ("OOps: Something Else",err)
      else:
        print(db_response.text)
